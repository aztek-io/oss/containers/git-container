FROM alpine

LABEL maintainer="robert@scriptmyjob.com"

RUN apk update

RUN apk add git

RUN git --version | \
        grep -Eow '([0-9]+\.){2}[0-9]+' | \
        head -n 1 > /version.out

